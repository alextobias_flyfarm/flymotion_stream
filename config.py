import json, jsonpickle

file_name = "config.JSON"

def LoadConfig():
    file = open(file_name, "r")
    loaded = jsonpickle.decode(file.read())
    return loaded

def ResetDefaults():
    config = Config()
    config.Init()
    config.SaveConfig()

class Config(object):

    def Init(self):
        self.rows = [0, 1, 2, 3, 4, 5, 6, 7]
        self.ports = ["?"] * len(self.rows)
        self.nodeCount = [30, 30, 30, 30, 30, 30, 30, 30]

        self.dbname = "temps"
        self.table = "sensorlog"

        self.host = "localhost"
        self.dbport = 3306
        self.user = "remote"
        self.password = "flyfarm"

        self.cloud_host = ""
        self.cloud_user = ""
        self.cloud_password = ""

        self.cloud_dbport = 33061
        self.cloud_uploadIntervalSeconds = 20

    def ToJSON(self):
        return jsonpickle.encode(self)

    def SetPort(self, portID, portName):
        self.ports[portID] = portName
        self.SaveConfig()

    def SetDBPort(self, portName):
        self.dbport = portName
        self.SaveConfig()

    def SetCloudDBPort(self, portName):
        self.cloud_dbport = portName
        self.SaveConfig()

    def SetTable(self, _table):
        self.table = _table
        self.SaveConfig()

    def SetHost(self, _host):
        self.host = _host
        self.SaveConfig()

    def SetCloudHost(self, _host):
        self.cloud_host = _host
        self.SaveConfig()

    def SetDBName(self, _dbname):
        self.dbname = _dbname
        self.SaveConfig()

    def SetUser(self, _user):
        self.user = _user
        self.SaveConfig()

    def SetPassword(self, _password):
        self.password = _password
        self.SaveConfig()

    def SetCloudUser(self, _user):
        self.cloud_user = _user
        self.SaveConfig()

    def SetCloudPassword(self, _password):
        self.cloud_password = _password
        self.SaveConfig()

    def SetCloudBackupInterval(self, nodeCount):
        self.cloud_uploadIntervalSeconds = int(nodeCount)
        self.SaveConfig()

    def SetNodeCount(self, nodeCount):
        nodeCount = nodeCount.split(',')
        self.nodeCount = nodeCount
        self.SaveConfig()

    def SaveConfig(self):
        with open(file_name, 'w') as text_file:
            text_file.write(self.ToJSON())
        print("Saved Config")

    def ShowCreds(self):
        print("Host: \t\t" + self.host)
        print("Database: \t" + self.dbname)
        print("Port: \t\t" + self.dbport)
        print("User: \t\t" + self.user)

    def ShowCloudCreds(self):
        print("Host: \t\t" + self.cloud_host)
        print("Database: \t" + self.dbname)
        print("Port: \t\t" + self.cloud_dbport)
        print("User: \t\t" + self.cloud_user)

    def ToString(self):
        print("Table: "+ self.table)

        s = "Port Assignments\n"
        for x in range(0, len(self.rows)):
            s = s + "Row: " + str(self.rows[x]) + " -> " + str(self.ports[x]) + "\n"

        print("Nodes in each chain:")
        for i in range(0, len(self.nodeCount)):
            print(str(i) + ": " + str(self.nodeCount[i]))

        return s
