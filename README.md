# Fly Motion Capture

This program opens up an attached webcam and records changes in movement.


##Usage

From the command line, navigate to the project directory and run `python main.py`.

I've implemented command line argument support, a full list of which can be found by running `python main.py -h`.


### Modes

There are 3 modes. The mode used can be selected from the command line. 

* Mode 1: Visualize, log and upload to server.
* Mode 2: Visualizes but does not upload.
* Mode 3: Takes the visualization and saves it to a video **output.avi** in the directory folder.


### Optional Command Line Arguments


| Argument        | Description | Defaults to |
| --------------- | ----------- | ----------- |
|`-m *1, 2 or 3*` | Sets mode. See section on modes for more info.| 2 (no-upload mode) |
|`-video *path*`  | Use a video file instead of camera. Provide path to the video. | None (not used) |
|`-src *number*`   | Select the webcam to use. Typically will be 0 or 1. | 0 |
|`-fps *number*`  | If outputing as video, choose video framerate. | 30 fps |
|`-x *num1* *num2*`| Slice each frame horizontally from num1 to num2. | None (not used by default) |
|`-y *num1* *num2*`| Slice each frame vertically from num1 to num2. | None (not used by default) |
|`-r *angle (deg)*` | Rotate the video stream clockwise by the specified degrees. | 0 deg |
|`-w *size (px)*`   | After rotating and slicing, resize the frame proportionally to specified width. | 480 px |
|`-b *value*` | Gaussian Blur value. Used for processing. Higher is more blurry. | 5 |
|`-t *value*` | Threshold value. Used for processing. Higher means lighter grays will be turned to black, and vice versa. | 12|
|`-a *size (px)*` | Min area size to be considered a valid contour. | 0 px |
|`-A *size (px)*` | Max area size to be considered a valid contour. | 0 px |
|`-n *number*` | Number of samples to average over each period of time | 100 |

Example of using all arguments together:

`python main.py -m 1 -src 1 -x 40 520 -y 80 440 -r 270 -w 400 -b 11 -t 20 -a 50 -A 160 -n 200`

or 

`python main.py -m 3 -src 0 -fps 30`






