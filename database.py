import pymysql.cursors, time

class DBManager():

    def OpenSSHTunnel(self):
        with sshtunnel.SSHTunnel('128.199.191.249', 'workshop', '22', '/home/pi/.ssh/id_rsa', '33061') as tunnel:
            print ("Connected on port {} at {}".format(tunnel.local_port, tunnel.local_host))


    def Open(self):
        #ssh -L 33061:localhost:3306 workshop@128.199.191.249 -f -N

        #print("Opening db connection")
        #print(self.host + "\n" + str(self.port) + "\n" + self.user + "\n" +self.password + "\n" +self.dbname)

        self.db = pymysql.connect(host="localhost",
                             port=self.port,
                             user=self.user,
                             password=self.password,
                             db=self.dbname)


        self.cursor = self.db.cursor()


    def Close(self):
        self.db.close()

    def SetCredentials(self, _host, _port, _dbname, _user, _password, _tableName):
        self.host = _host
        self.port = int(_port)
        self.dbname = _dbname
        self.user = _user
        self.password = _password
        self.table = _tableName


    def CreateSensorTable(self, _tableName):
        self.table = _tableName
        self.Open()

        import warnings
        try:
            with warnings.catch_warnings():
                warnings.filterwarnings("error", category=sql.Warning)
                self.cursor.execute("CREATE TABLE IF NOT EXISTS " + self.table + "(LogTime datetime, RowIndex int, SensorIndex int, Temperature1 real, Humidity1 real, Temperature2 real, Humidity2 real)")
        except sql.Warning as w:
            code, msg = w.args
            if code == 1050:
                # Table 'sensorlog' already exists
                # This is a harmless warning which we can safely ignore
                pass
            else:
                # Uncatch all other warnings
                warnings.warn(w)

        self.db.commit()
        self.Close()

    def ClearTable(self):
        self.Open()
        self.cursor.execute("DELETE FROM " + self.table)
        self.db.commit()
        print("Table Cleared")
        self.Close()

    def AddData(self):
        self.Open()
        # Insert a row of data
        for i in range(0, 20):
            self.cursor.execute("INSERT INTO " + self.table + " VALUES (CURRENT_TIMESTAMP, " + str(i) + " , 4.3, 0)")

        self.db.commit()
        self.Close()


    def LogSensorValues(self, serialData):
        delay = time.time()
        #return True
        self.Open()

        if (serialData[1] == CommCode.SENSORDATA):


            reportstring = ""
            for p in serialData:
                if (p == ''):
                    print("Error: " + serialData[0] + " not a valid sensor log")
                    return False

                #print(p)


            rowID = str(serialData[2])
            sensorIdx = str(serialData[3])
            temp1 = str(serialData[4])
            hum1 = str(serialData[5])
            temp2 = str(serialData[6])
            hum2 = str(serialData[7])

            reportstring = rowID + ", " + sensorIdx + ", " + temp1 + ", " + hum1 + ", " + temp2 + ", " + temp2 + ", " + hum2

            query = "INSERT INTO " + self.table + " (LogTime, RowIndex, SensorIndex, Temperature1, Humidity1, Temperature2, Humidity2) VALUES (CURRENT_TIMESTAMP, " + rowID + " , " + sensorIdx + " , " + temp1 + ", " + hum1 + " , " + temp2 + ", " + hum2 +")"

            try:
                #self.cursor.execute("INSERT INTO " + self.table + " VALUES (CURRENT_TIMESTAMP, " + rowID + " , " + sensorIdx + " , " + temp1 + ", " + hum1 + " , " + temp2 + ", " + hum2 +")")
                self.cursor.execute(query)
                self.db.commit()
                #print("Data Logged: " + reportstring)
            except:
                print("Error: the database is being rolled back")
                self.cursor.rollback()
        else:
            print("Not sensordata: " + serialData[0])

        self.Close()
        #print("Added data to " + self.host + " database: " + str(float(time.time() - delay)))
        return True

    def AddData(self, values):
        #self.cursor.execute("INSERT INTO sensorlog VALUES (CURRENT_TIMESTAMP, " + str(values[0]) + " , " + str(values[1]) + ", " + str(values[2]) + ")")
        print("AddData(value) not implemented yet")

    def AddTestData(self):
        testData = ["This is test data", CommCode.SENSORDATA, "99", "77", "20.0", "30.0", "40.0", "50.0"]
        self.LogSensorValues(testData)

    def AddRecords(self, _records):
        self.Open()
        for r in _records:
            try:
                query = "INSERT INTO sensorlog (logIndex, LogTime, RowIndex, SensorIndex, Temperature1, Humidity1, Temperature2, Humidity2) VALUES (" + str(r[0]) + ", '" + str(r[1]) + "' , " + str(r[2]) + " , " + str(r[3]) + ", " + str(r[4]) + " , " + str(r[5]) + ", " + str(r[6]) + ", " + str(r[7]) +")"
                #self.cursor.execute("INSERT INTO sensorlog VALUES (" + str(r[0]) + ", " + str(r[1]) + " , " + str(r[2]) + " , " + str(r[3]) + ", " + str(r[4]) + " , " + str(r[5]) + ", " + str(r[6]) + ", " + str(r[7]) + ")")
                self.cursor.execute(query)
                self.db.commit()
                #print("Data Logged: " + reportstring)
            except:
                print("Error: the database is being rolled back")
                self.cursor.rollback()

        self.Close()
        return

    def GetRecordsFrom(self, _secondsAgo):
        self.Open()
        list = self.cursor.execute("SELECT * FROM " + self.table + " WHERE `LogTime` > DATE_SUB(NOW(), INTERVAL " + str(_secondsAgo) + " SECOND) GROUP BY `Logtime`")
        list = self.cursor.fetchall()
        #for reading in self.cursor.fetchall():
            #print (str(reading[0]) + " " + str(reading[1]) + " " + str(reading[2]) + " " + str(reading[3]) + " " + str(reading[4]) + " " + str(reading[5]))
        self.Close()
        return list

    def DumpData(self):
        self.Open()
        print("Dumping sensorlog Data")
        list = self.cursor.execute("SELECT * FROM " + self.table)
        #for row in list:
            #print(row)
        #print(list)
        for reading in self.cursor.fetchall():
            print (str(reading[0]) + " " + str(reading[1]) + " " + str(reading[2]) + " " + str(reading[3]) + " " + str(reading[4]) + " " + str(reading[5]))

        self.Close()

    def new_addTable(self, _tableName):
        self.table = _tableName
        self.Open()

        self.cursor.execute("CREATE TABLE IF NOT EXISTS " + self.table + "(LogTime datetime, Movement int)")


        self.db.commit()
        self.Close()

    def new_addData(self, movementData):
        self.Open()
        query = "INSERT INTO " + self.table + " (LogTime, Movement) VALUES (CURRENT_TIMESTAMP, '" + str(movementData) +"')"
        self.cursor.execute(query)
        self.db.commit()
        self.Close()
