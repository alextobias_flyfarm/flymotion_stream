#main.py

from imutils.video import VideoStream
import argparse
import datetime
import imutils
import time
import cv2
import config, database
from collections import deque
import numpy as np
from math import ceil


#------------------------------

OFFSET_FROM_BOT = 50
OFFSET_FROM_TOP = 50
VERTICAL_STRETCH = 10

#------------------------------


#command line arguments
ap = argparse.ArgumentParser()

ap.add_argument("-m", "--mode", type = int, choices=[1,2,3], default=2, help="Video Mode. 1 will log and upload, 2 (default) will not log, 3 will record and save video.")

ap.add_argument("-video", "--video", help="path to the video file")
ap.add_argument("-src", "--src", type=int, default=0, help="Video source number. May be 0 or 1, defaults to 0.")
ap.add_argument("-fps","--fps", type=int, default=30, help="Output video framerate. Default is 30.")

ap.add_argument('-x', type=int, nargs = 2, help="slices the frame, x values, horizontally")
ap.add_argument('-y', type=int, nargs = 2, help="slices the frame, y values, vertically")
ap.add_argument("-r", "--rotate", type=int, default=0, help="Angle of rotation. Defaults to 270 since webcam is assumed sideways.")
ap.add_argument("-w", "--width", type=int, default=480, help="frame resize width")

ap.add_argument("-b", "--blur_val", type=int, default=5, help="Gaussian Blur value, default is 5, I think it has to be an odd number.")
ap.add_argument("-t", "--thresh_val", type=int, default=12, help="B/W Threshold Value, default is 12.")
ap.add_argument("-a", "--area_min", type=int, default=0, help="minimum area size, default is 0")
ap.add_argument("-A", "--area_max", type=int, default=225, help="max area size, default is 225")

ap.add_argument("-n", "--num_samples", type=int, default=100, help="Number of samples to average over each period of time.")



args = ap.parse_args()



if args.video is None:								#pick if it's a video to load, or a live camera stream
	vs = VideoStream(src=args.src).start()
else:
	vs = VideoCapture(args.video)


if(args.mode==1):									#if we're in logging mode, let's connect to the database
	config = config.LoadConfig()
	backupdb = database.DBManager()
	backupdb.SetCredentials(config.cloud_host, config.cloud_port, config.cloud_dbname, "flyfarmuser", "flyfarm2", "fly_movement")
	#backupdb.new_addTable("fly_movement")

if(args.mode==3):									#if we're in recording mode, let's set up video output
	fourcc = cv2.VideoWriter_fourcc('M','J','P','G')
	out = cv2.VideoWriter('output.avi', fourcc, args['fps'], (fwidth,fheight))


#some other stuff should go here for setting up the frame size and shape, rotate stuff

f_prev = None

test_frame = vs.read()
test_frame = test_frame if args.video is None else test_frame[1]
test_frame = imutils.rotate(test_frame,args.rotate)
if(args.x is not None):
	test_frame = test_frame[:,args.x[0]:args.x[1]]
if(args.y is not None):
	test_frame = test_frame[args.y[0]:args.y[1],:]
test_frame = imutils.resize(test_frame, width=args.width)

frame_width = test_frame.shape[1]
frame_height = test_frame.shape[0]

movement = deque()
for i in range(args.num_samples):
	movement.append(0)


movement_graph = deque(maxlen=frame_width)
for i in range(frame_width):
	movement_graph.append(1)

popped_value = 0
movement_sum = 0

sample_point_size = ceil(frame_width/args.num_samples)
print("Sample point width: %s" % sample_point_size)



print("Dimensions: {}(w) x {}(h)".format(frame_width,frame_height))

output_counter = 0


while True:
	
	#grab a frame
	frame = vs.read()
	frame = frame if args.video is None else frame[1]

	if frame is None:
		break

	#let's apply our transformations on it
	#ideal order is rotate -> slice -> rezize

	frame = imutils.rotate(frame,args.rotate) 	#rotate

	if(args.x is not None):						#slice
		frame = frame[:,args.x[0]:args.x[1]]
	if(args.y is not None):
		frame = frame[args.y[0]:args.y[1],:]	#frame = frame[args.y[0]:args.y[1],args.x[0]:args.x[1]]
	

	frame = imutils.resize(frame, width=args.width)

	f_final = frame.copy()


	#perform processing on frame
	#grayscale -> blur -> get difference(delta) -> threshold -> dilate -> find contours

	f_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	f_gray = cv2.GaussianBlur(f_gray, (args.blur_val, args.blur_val), 0)	#gray and blur frames


	if f_prev is None:
		f_prev = f_gray
		continue

	f_delta = cv2.absdiff(f_prev, f_gray)		#compare frames

	if cv2.countNonZero(f_delta) == 0:
		continue								#if there's no differences at all, skip

	f_thresh = cv2.threshold(f_delta, args.thresh_val, 255, cv2.THRESH_BINARY)[1]

	f_dilate = cv2.dilate(f_thresh, None, iterations=2)

	cnts = cv2.findContours(f_dilate.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
	cnts = cnts[0] if imutils.is_cv2() else cnts[1]

	contours = 0

	for c in cnts:
		if (cv2.contourArea(c) < args.area_min) or (args.area_max < cv2.contourArea(c)):
			continue

		(r_x, r_y, r_w, r_h) = cv2.boundingRect(c)
		cv2.rectangle(f_final, (r_x, r_y), (r_x + r_w, r_y + r_h), (0, 255, 0), 2)
		contours +=1

	movement.append(contours)
	popped_value = movement.popleft()

	movement_sum += contours
	movement_sum -= popped_value
	movement_average = movement_sum/len(movement)

	graph_mode = 2

	if(graph_mode == 1):
		sample_point = frame_height * (1-contours/(max(movement)+1))
	if(graph_mode == 2):
		sample_point = frame_height-contours*VERTICAL_STRETCH-OFFSET_FROM_BOT
	# if(sample_point >= (frame_height-OFFSET_FROM_BOT)):
	# 	sample_point = frame_height-OFFSET_FROM_BOT
	# if(sample_point <= OFFSET_FROM_TOP):
	# 	sample_point = OFFSET_FROM_TOP

	for i in range(sample_point_size):
		movement_graph.append(sample_point)

	#draw text and timestamp on frame

	movement_array = np.asarray(movement_graph)

	x_array = np.arange(frame_width)

	pts = np.vstack((x_array,movement_array)).astype(np.int32).T

	cv2.polylines(f_final, [pts], isClosed=False, color=(0,0,255))
	
	cv2.putText(f_final, "Queue length: {}, Movement Sum: {}".format(str(len(movement)), movement_sum),   (10, 80), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
	cv2.putText(f_final, "Flies Active:   {}".format(contours), (10, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
	cv2.putText(f_final, "Moving Average: {}".format(round(movement_average,1)),   (10, 40), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
	cv2.putText(f_final, datetime.datetime.now().strftime("%A %d %B %Y %I:%M:%S%p"), (10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1)

	cv2.imshow("Live Feed", f_final)
	cv2.imshow("Gray/Blur", f_gray)
	cv2.imshow("Threshold", f_thresh)
	cv2.imshow("Dilate", f_dilate)
	cv2.imshow("Frame Delta", f_delta)


	output_counter+=1

	if(output_counter % 30 == 0):

		print("---- Timestamp: %s" % datetime.datetime.now().strftime("%A %d %B %Y %I:%M:%S%p"))
		print("> Flies Active:   {}".format(contours))
		print("> Moving Average: {}".format(round(movement_average,1)))
		print("> Queue length: {}, Movement Sum: {}".format(str(len(movement)), movement_sum))

	f_prev = f_gray


	if(args.mode==1):
		backupdb.new_addData(THING_TO_UPLOAD)


	key = cv2.waitKey(1) & 0xFF

	# if the `q` key is pressed, break from the lop
	if key == ord("q"):
		break




vs.stop() if args.video is None else vs.release()
#args.get("video", None) is None else vs.release()
cv2.destroyAllWindows()


#we want to take the last n = 50 recordings, and compare the current measurement against the last average.

#every 10 minutes, we'll take the average of the recordings

#looks like:
#-----------------------------------------
#|
#| Flies Active: 
#| Current Average:
#| Activity Level: 
#|
#|               /\    __  _
#|             _/  \__/  \/ \
#|       _____/              \
#|      /                     \/\_
#|   __/                          \
#|  /
#-----------------------------------------

	# h,w,_ = f_final.shape
	# x = np.arange(w)

	# B = f_final[:,:,0].sum(axis=0)
	# B = h - h * B / B.max()
	# G = f_final[:,:,1].sum(axis=0)
	# G = h - h * G / G.max()
	# R = f_final[:,:,2].sum(axis=0)
	# R = h - h * R / R.max()

	# pts = np.vstack((x,B)).astype(np.int32).T
	# cv2.polylines(f_final, [pts], isClosed=False, color=(255,0,0))
	# pts = np.vstack((x,G)).astype(np.int32).T
	# cv2.polylines(f_final, [pts], isClosed=False, color=(0,255,0))
	# pts = np.vstack((x,R)).astype(np.int32).T
	# cv2.polylines(f_final, [pts], isClosed=False, color=(0,0,255))

#--------------------------------------------